<?php

namespace Zephyr\EditableBundle\Utils;

/**
 * Description of Fichier
 *
 * @author Nicolas de Marqué <ndm@zephyr-web.fr>
 */
class Fichier {
	protected $chemin;
	
	public function __construct($chemin){
		$this->chemin = $chemin;
		if(!file_exists($chemin))
			throw new Exception("Fichier non existant: $chemin");
		
		$this->creerDossier();
	}
	
	public function creerDossier(){
		$this->dossier = $this->chemin.'.sav';
		echo $this->dossier;
		if(!file_exists($this->dossier))
			mkdir($this->dossier);
	}
	
		
	public function sauver($text, $user){
		$text = utf8_decode($text);
		$nom = $this->dossier .'/'. $user .'_'.microtime(true);
		file_put_contents($nom, $text);
		file_put_contents($this->chemin, $text);
		return true;
	}
	public function getVersion($chemin, $user){
		
	}
	public function getVersions($chemin, $user){
		
	}
}

?>
