Editable
========

Installation
------------

## Composer

"zephyr/editable": "2.2.*@dev"

## AppKernel

$bundles[] = new Zephyr\EditableBundle\ZephyrEditableBundle();

## Annotation

use Zephyr\EditableBundle\Annotations\Page;
/**
 * @Page();
 */