<?php
// MyBundle/Annotation/MyAnnotation.php
namespace Zephyr\EditableBundle\Annotations;

/**
 * @Annotation
 * Paramètres: 
 *    securite={role=role1, roles={role2,role3}}
 */
class EditablePage {
	
	public $editable = false;
   
	public function __construct(array $data=null) {
		if(isset($data['value']))
			$this->editable = $data['value'];
		else
			$this->editable = true;
	}
}