/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.language = 'fr';
	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.plugins = 'dialogui,'+
		'dialog,'+
		//'about,'+
		'a11yhelp,'+
		'dialogadvtab,'+
		//'basicstyles,'+
		//'bidi,'+
		'blockquote,'+
		//'clipboard,'+
		'button,'+
		'panelbutton,'+
		'panel,'+
		'floatpanel,'+
		'colorbutton,'+
		//'colordialog,'+
		//'templates,'+
		'menu,'+
		'contextmenu,'+
		'div,'+ 
		'resize,'+
		'toolbar,'+
		'elementspath,'+
		'list,'+
		'indent,'+
		'enterkey,'+
		'entities,'+
		'popup,'+
		'filebrowser,'+
		'find,'+
		'fakeobjects,'+
		'flash,'+
		'floatingspace,'+
		'listblock,'+
		'richcombo,'+
		//'font,'+
		'forms,'+
		'format,'+
		'htmlwriter,'+
		'horizontalrule,'+
		//'iframe,'+
		'wysiwygarea,'+
		'image,'+
		'smiley,'+
		//'justify,'+
		'link,'+
		'liststyle,'+
		'magicline,'+
		'maximize,'+
		//'newpage,'+
		'pagebreak,'+
		'pastetext,'+
		//'pastefromword,'+
		//'preview,'+
		//'print,'+
		'removeformat,'+
		'save,'+
		'selectall,'+
		'showblocks,'+
		'showborders,'+
		//'sourcearea,'+
		//'sourcedialog,'+
		'specialchar,'+
		'menubutton,'+
		'scayt,'+
		'stylescombo,'+
		'tab,'+
		'table,'+
		'tabletools,'+
		'undo,'+
		//'wsc,'+
		'stylesheetparser,'+
		//'z_versionning'+
		'';
	/*config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo', 'save' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'save' },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];*/

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';
	config.scayt_autoStartup = true;
	config.scayt_sLang ="fr_FR";
	//config.contentsCss =  'assets/sample.css';

					// Do not load the default Styles configuration.
	/*config.stylesSet = [];*/
	
};
CKEDITOR.disableAutoInline = true;
	CKEDITOR.stylesSet.add( 'default',
	[
		// Block Styles
		//{ name : 'CSS Style', element : 'span', attributes : { 'class' : 'my_style' } },
		// Object Styles
		//{ name : 'Mini Titre', element : 'span', attributes : { 'class' : 'mini_titre' } },
	]);

