<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nuks
 * Date: 16/01/13
 */

namespace Zephyr\EditableBundle\Twig\Extension;

class CkeditorExtension extends \Twig_Extension {

	/**
	 *
	 * @var \Twig_Environment 
	 */	
	public $env;
	
	public function initRuntime(\Twig_Environment $environment)
	{
		$this->env = $environment;
	}

	public function getFunctions()
	{
		return array(
			'ckeditor' => new \Twig_Function_Method($this, 'ckeditor', array('is_safe' => array('html'))),
		);
	}

	public function ckeditor($option, $type = "replace", $element = null, array $parameters = array())
	{
		switch($option){
			case "init":
				return $this->init($parameters);
			break;
			case "active":
				return $this->env->render("ZephyrEditableBundle:ckeditor:replace.html.twig", array('type' => $type, 'element' => $element, 'parameters' => $parameters));
			break;
			default:
				throw new \Exception("Ckeditor ne peux être appelé avec \"$option\" comme premier argument.");
		}

		return null;
	}
	
	public function getLibs($parameters){
		
		$libs = array();
		//TODO: ajouter la gestion des langues
		$libs[] = "bundles/zephyreditable/ckeditor4/ckeditor/ckeditor.js";
		
		return $libs;
	}

	/**
	 * Set to true 
	 * @var type 
	 */
	protected $premier = false;
	/**
	 * Utiliser pour ajouter du contenu editable
	 * @param array $parameters
	 * @return string
	 */
	public function libs($parameters=array()){		
		if($this->premier) return;
		return $this->env->render(
				"ZephyrEditableBundle:ckeditor:init.html.twig",
				array('libs' => $this->getLibs($parameters)));
	}

	/**
	 * Utiliser pour ajouter du contenu editable
	 * @param array $parameters
	 * @return string
	 */
	public function contentEditableTemplate($id){
		return $this->env->render(
				"ZephyrEditableBundle:ckeditor:content_editable_template.html.twig",
				array('id' => $id));
	}

	public function getName()
	{
		return 'twig.extension.editable.ckeditor';
	}
}
