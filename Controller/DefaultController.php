<?php

namespace Zephyr\EditableBundle\Controller;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Zephyr\EditableBundle\EventListener\KernelListener;
use Zephyr\EpouseMoiBundle\Fichier;

class DefaultController extends Controller
{
	/**
	 *
	 * @var type 
	 */
	protected $locator;

	/**
     * @Route("/save_editable/", name="save_editable")
     */
    public function saveAction()
    {
		/* @var $context \Symfony\Component\Security\Core\SecurityContext */
		$context = $this->container->get('security.context');
		if($context->getToken()==null)
			$user = "anonymous";
		else
			$user = $this->get('security.context')->getToken()->getUser();

		$text = utf8_encode(html_entity_decode($this->getRequest()->get('text')));
		$id = $this->getRequest()->get('id');
		$templates = $this->getRequest()->getSession()->get(KernelListener::session_name);
		
		$template = $templates[$id]['template'];

		$this->get('zephyr.editable_page.kernel_listener')->testSecurite();			
					
		try{
			$fichier = new \Zephyr\EditableBundle\Utils\Fichier($this->container->get('templating.locator')->locate($template));
			$fichier->sauver($text, $user);
			return new Response('Sauvegardé.', 200);
		}catch(\Exception $e)
		{			
			return new Response('Un problème est survenu.'.$e->getMessage(), 503);
		}
    }

}
