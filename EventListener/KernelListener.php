<?php

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Zephyr\EditableBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Zephyr\EditableBundle\Twig\Extension\CkeditorExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * WebDebugToolbarListener injects the Web Debug Toolbar.
 *
 * The onKernelResponse method must be connected to the kernel.response event.
 *
 * The WDT is only injected on well-formed HTML (with a proper </body> tag).
 * This means that the WDT is never included in sub-requests or ESI requests.
 *
 */
class KernelListener
{
	const DISABLED        = 0;
	const ENABLED         = 1;
	const ENABLED_MINIMAL = 2;
	
	const session_name = 'zephyr_editable_templates';
	
	protected $twigEngine;
	protected $twigExtension;
	protected $controllerListener;
	protected $configuration;
	protected $interceptRedirects;

	protected $response;
	
	public function __construct(
									ContainerInterface $container,
									TwigEngine $twigEngine, 
									CkeditorExtension $twigExtension,
									ControllerListener $controllerListener,
									array $configuration,
									$interceptRedirects = false)
	{		
		$this->container = $container;
		$this->twigEngine= $twigEngine;
		$this->twigExtension = $twigExtension;
		$this->controllerListener = $controllerListener;
		$this->configuration = $configuration;		
		$this->interceptRedirects = (Boolean) $interceptRedirects;
		$this->mode = $configuration['activated'];
		
	}

	public function isVerbose()
	{
		return self::ENABLED === $this->mode;
	}

	public function isEnabled()
	{
		return self::DISABLED !== $this->mode;
	}

	protected function filterEvents($event, Request $request, Response $response=null)
	{
		$annotation = $this->controllerListener->editablePage;

		if(false === $annotation)
			return true;
		/*if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType())
			return true;*/
		if ( $request->isXmlHttpRequest()) {
			return true;
		}
		
		if (self::DISABLED === $this->mode
//				|| !$response->headers->has('X-Debug-Token')
				|| ( isset($response) && '3' === substr($response->getStatusCode(), 0, 1))
				//||	(
				//$response->headers->has('Content-Type')
				//&& false === strpos($response->headers->get('Content-Type'), 'html')
				//)
				//|| 'html' !== $request->getRequestFormat()
		) {
				return true;
		}	
		return  ! $this->testSecurite();
	}
	/**
	 * Teste la sécurité
	 * @param array $annotation
	 * @return boolean true si l'utilisateur a des droits
	 */
	function testSecurite(){
		
		$annotation = $this->controllerListener->editablePage;
		/* 
		 * Teste la sécurité suivant la configuration
		 */
		if($this->configuration['securite']['activated']==false)
			return true;
		
		$securite = false;
		if(isset($this->configuration['securite']['role']) &&
				$this->container->get('security.context')->isGranted($this->configuration['securite']['role'])){
			return true;
		}
		if(isset($this->configuration['securite']['roles'])){
			foreach($this->configuration['securite']['roles'] as $role){
				if($this->container->get('security.context')->isGranted($role)){
					return true;
				}
			}
		}		
		/* 
		 * Teste la sécurité suivant l'annotation
		 */
		if(isset($annotation['securite']['role']) &&
				$this->container->get('security.context')->isGranted($annotation['securite']['role'])){
			return true;
		}
		if(isset($annotation['securite']['roles'])){
			foreach($annotation['securite']['roles'] as $role){
				if($this->container->get('security.context')->isGranted($role)){
					return true;
				}
			}
		}		

		return false;
	}
	
	public function onKernelView(GetResponseForControllerResultEvent $event){
		
		$request = $event->getRequest();
		
		if($this->filterEvents($event, $request))
			return;

		if($this->controllerListener->editablePage){
			$event->stopPropagation();
			$this->container->get('sensio_framework_extra.view.listener')->onKernelView($event);
			$content = $event->getResponse()->getContent();
			//$response = new Response();
			
			$id=  str_replace(".", "_", microtime (true));
			$route = $this->container->get('router')
                     ->generate('save_editable', array());
			$template = $request->attributes->get('_template');
			$libs = $this->container->get('zephyr.twig.extension.ckeditor')->libs();
			$scripts= $this->container->get('zephyr.twig.extension.ckeditor')->contentEditableTemplate($id);
			
			$session = $request->getSession();
			
			$old_session = $session->get('static::session_name') ;
			$old_session = is_array($old_session) ? $old_session: array();
			$templates = array_merge($old_session, array($id=>array('template'=>$template)));
			$session->set(static::session_name, $templates);

			
			$event->getResponse()->setContent(
				$libs.
				"<form id='form_$id'><div
					id='div_$id'
					contenteditable='true'
					data-route='$route'
					data_id='$id'
					data-callback='sauver_$id'
				>".$content."</div></form>".
				$scripts
			);
			//$response->setContent("hello".$content."hello");
			//$response->setContent(json_encode($event->getControllerResult()));
			//$response->setStatusCode($this->controllerListener->editablePage->code);
			//$event->setResponse($response);
		}		
	}

	public function onKernelResponse(FilterResponseEvent $event)
	{
		
		$request = $event->getRequest();
		$response = $event->getResponse();
		if($this->filterEvents($event, $request, $response))
			return;		
	}	
}
