<?php

namespace Zephyr\EditableBundle\EventListener;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Zephyr\EditableBundle\Annotations\EditablePage;

 
class ControllerListener {
	
	const annoEditablePageCName = "\Zephyr\EditableBundle\Annotations\EditablePage";

	public $editablePage = false;
	 
	private $reader;

	public function __construct(Reader $reader) {
		$this->reader = $reader;
	}
	 
	public function onCoreController(FilterControllerEvent $event) {
		$this->annotationReader($event); 
	}
	
	private function annotationReader(FilterControllerEvent $event)
	{
		if (!is_array($controller = $event->getController()))
			return;
 
		$this->editablePageAnnotationReader($controller[0], $controller[1]);
	}
	private function editablePageAnnotationReader($class, $method)
	{
		$this->editablePage = false;
				
		if(!$annotation = $this->reader->getMethodAnnotation(new \ReflectionMethod($class, $method), self::annoEditablePageCName))
			if(!$annotation = $this->reader->getClassAnnotation(new \ReflectionClass($class), self::annoEditablePageCName))
				return ;

		if(! $annotation instanceof EditablePage )
			throw new \Exception("Something go wrong with reader.");
		
		$this->editablePage = $annotation->editable;
		
	}
}